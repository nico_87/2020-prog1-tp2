﻿namespace Logica
{
    public class Informe
    {
        public string Legajo { get; internal set; }
        public string Nombre { get; internal set; }
        public int DiasTrabajados { get; internal set; }
        public int Inasistencias { get; internal set; }
        public int HorasMediasExtras { get; internal set; }
        public int Tardanzas { get; internal set; }
    }
}