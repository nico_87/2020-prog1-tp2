﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Turno
    {
        public int NroTurno { get; set; }
        public string Descripcion { get; set; }
        public List<int> Dias { get; set; }//ver que hacer con la propiedad me parece nuva clase
        public int Entrada { get; set; }
        public int Salida { get; set; }
        public int Estado { get; set; }//0:ACTICO---99:ELIMINADO

        internal void ModificarTurno(string descripcion, List<int> diasM, int horaDesde, int horaHasta)
        {
            if (descripcion != null)
            {
                Descripcion = descripcion;
            }
            if (diasM != null)
            {
                Dias.Clear();
                Dias = diasM;
            }
            if (horaDesde != 99)//ver
            {
                Entrada = horaDesde;
            }
            if (horaHasta != 99)
            {
                Salida = horaDesde;
            }
        }

        public string ObtenerDiaHoraTurno(int dia)
        {
            foreach (var item in Dias)
            {
                if (item == dia)
                {
                    switch (dia)
                    {
                        case 0:
                            return "Turno:  " + NroTurno + "Desde: " + Entrada + "Hasta: " + Salida + "Día: Domingo";
                        case 1:
                            return "Turno:  " + NroTurno + "Desde: " + Entrada + "Hasta: " + Salida + "Día: Lunes";
                        case 2:
                            return "Turno:  " + NroTurno + "Desde: " + Entrada + "Hasta: " + Salida + "Día: Martes";
                        case 3:
                            return "Turno:  " + NroTurno + "Desde: " + Entrada + "Hasta: " + Salida + "Día: Miercoles";
                        case 4:
                            return "Turno:  " + NroTurno + "Desde: " + Entrada + "Hasta: " + Salida + "Día: Jueves";
                        case 5:
                            return "Turno:  " + NroTurno + "Desde: " + Entrada + "Hasta: " + Salida + "Día: Viernes";
                        case 6:
                            return "Turno:  " + NroTurno + "Desde: " + Entrada + "Hasta: " + Salida + "Día: Sabado";
                    }
                    
                }
            }
            return "No se encontraron coincidencias.";
        }
    }
}
