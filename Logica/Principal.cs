﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Logica
{
    public class Principal
    {
        public List<Empleado> Empleados { get; set; }
        public List<Turno> Turnos { get; set; }
        public List<Asistencia> Asistencias { get; set; }
        public List<Informe> InformeEmpleado { get; set; }
        public List<InformeIngreso> InformeIngresos { get; set; }
        

        public Principal()
        {
            CrearArchivos();
            Empleados = ArchivoEmpleados();
            Turnos = ArchivoTurnos();
            Asistencias = ArchivoAsistencias();
        }



        public List<string> ListadoEmpleadosNombr()
        {
            List<string> Nombres = new List<string>();
            foreach (var item in Empleados)
            {
                Nombres.Add(item.NombreApellido);
            }
            return Nombres;
        }

        /// <summary>
        /// EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEMPLEADOS
        /// </summary>

        public void AltaEmpleado(string nombre, string domicilio, string localidad, string telefono, string correoe, double sueldo, int turno, int estado)
        {
            Empleado nuevoEmpleado = new Empleado();
            nuevoEmpleado.Legajo = Convert.ToString(Empleados.Count+1000);
            nuevoEmpleado.NombreApellido = nombre;
            nuevoEmpleado.Domicilio = domicilio;
            nuevoEmpleado.Localidad = localidad;
            nuevoEmpleado.Telefono = telefono;
            nuevoEmpleado.CorreoElec = correoe;
            nuevoEmpleado.SueldoNeto = sueldo;
            nuevoEmpleado.Turno = ObtenerTurno(turno);//VER SI SE HACE METODO DE TURNO VALIDO O CON ESTE YA CUMPLE EL REQUISITO VER MODIFICAION TMB
            nuevoEmpleado.Estado = estado;

            Empleados.Add(nuevoEmpleado);
            GuardarEmpleados();
        }

        public Empleado ObtenerEmpleado(string legajo)
        {                         
            return Empleados.First(x => x.Legajo == legajo); 
        }

        public Empleado ObtenerEmpleadoPorNombre(string nombre)
        {
            return Empleados.First(x => x.NombreApellido == nombre);
        }

        public bool ModificarEmpleado(string legajo, string nombre, string domicilio, string localidad, string telefono,  string correo,double sueldo,int turno, int estado)
        {
            Empleado empleadoAModificar = ObtenerEmpleado(legajo);
            empleadoAModificar.ModificarEmpleado(nombre,domicilio,localidad,telefono,correo,sueldo);
            if (empleadoAModificar !=  null)
            {
                empleadoAModificar.Turno = ObtenerTurno(turno);
                return true;
            }
            return false;
        }

        public bool EliminarEmpleado(string legajo)
        {

            Empleado empleadoAEliminar = ObtenerEmpleado(legajo);
            bool noseusa = true;
            if (empleadoAEliminar.EstaTrabajando(Asistencias,ref noseusa) == false)//ver el false de parametro
            {
                empleadoAEliminar.Estado = 99;
                GuardarEmpleados();
                return true;
            }
            return false;
        }

        public List<string> BusquedaEmpeladoParcialOLegajo(string legajo, string nombre)
        {
            List<string> empleadosEncontrados = new List<string>();

            if (legajo!=null)
            {
                Empleado empleado = ObtenerEmpleado(legajo);
                empleadosEncontrados.Add((empleado.Legajo+" "+(empleado.NombreApellido)));
                return empleadosEncontrados;
            }
            else
            {
                List<string> legajoYNombre = new List<string>();
                foreach (var item in Empleados)
                {
                    legajoYNombre.Add(item.Legajo+"  "+item.NombreApellido);
                }
                return legajoYNombre.FindAll(delegate (string current) { return current.Contains(nombre); });
            }
        }

   





        /// <summary>
        /// TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTURNOS
        /// </summary>

        //REGISTRO TURNO
        public void RegistrarNuevoTurno(string descripcion, List<int> dias, int hasta,int desde,int estado)
        {
            Turno nuevoTurno = new Turno();
            nuevoTurno.Descripcion = descripcion;
            nuevoTurno.NroTurno = Empleados.Count+1 ;
            nuevoTurno.Dias = dias;
            nuevoTurno.Entrada = desde;
            nuevoTurno.Salida = hasta;
            nuevoTurno.Estado = estado;

            Turnos.Add(nuevoTurno);
            GuardarTurnos();
        }
        
        public bool ModificacionTurno(int numTurno, string descripcion, List<int> diasM, int horaDesde, int horaHasta)
        {
            Turno turnoEncontrado = ObtenerTurno(numTurno);
            if (turnoEncontrado == null)
            {
                return false;
            }
            else
            {
                turnoEncontrado.ModificarTurno(descripcion, diasM, horaDesde,horaHasta);
                GuardarTurnos();
                return true;
            }         
        }
        
        //metodo auxiliar
        private Turno ObtenerTurno(int numeroTurno)
        {
            return Turnos.First(x => x.NroTurno == numeroTurno);
        }

        public string BusquedaDeTurnos(string descripcion, int dia)
        {
            Turno turno = Turnos.First(x => x.Descripcion == descripcion);
            
            return turno.ObtenerDiaHoraTurno(dia);
        }

        public bool EliminarTurno(int numeroTurno)
        {
            Turno turnoEncontrado = ObtenerTurno(numeroTurno);
            if (turnoEncontrado != null)
            {
                if (Empleados.Exists(x=>x.Turno == turnoEncontrado)==false)
                {
                    turnoEncontrado.Estado = 99;
                    Turnos.Remove(turnoEncontrado);
                    return true;
                }      
            }
            return false;
        }

        






        /// AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASISTENCIAS
       

        //REGISTRO DE ASISTENCIA
        public void RegistrarAsistencia(string nombre,ref bool noDebeTrabajar,ref bool yaTrabajo)
        {
            Empleado empleadoEncontrado = ObtenerEmpleadoPorNombre(nombre);
            if (empleadoEncontrado!=null)
            {
                if (empleadoEncontrado.DiaLaboral(DateTime.Today)==true)
                {             
                    if (empleadoEncontrado.EstaTrabajando(Asistencias,ref yaTrabajo)==false)//si no esta trabajando creamos nueva asis
                    {
                        Asistencia registroAsistencia = new Asistencia();
                        registroAsistencia.Legajo = empleadoEncontrado.Legajo;
                        registroAsistencia.Ingreso = DateTime.Now;


                        Asistencias.Add(registroAsistencia);
                        GuardarAsistencias();
                    }
                    else
                    {
                        if (yaTrabajo==false)
                        {
                            GuardarAsistencias();
                        }
                        
                    }
                }
                else
                {
                    noDebeTrabajar = true;//no debe trabajar
                }               
            }
        }

        







        /// <summary>
        /// DATOS PARA INFORMES
        /// </summary>
        /// 

        //si cumple las dos condiciones de debe y no asiste cuenta una inasistencia LISTO------------------------------------------------
        public int CantidadInasistencia(string legajo, DateTime fechaDesde, DateTime fechaHasta)
        {
            Empleado empleadoEncontrado = ObtenerEmpleado(legajo);
            DateTime fechaParticular = fechaDesde;
            int contadorInasistencia = 0;
            while (fechaParticular >= fechaDesde & fechaParticular <= fechaHasta)
            {
                if (empleadoEncontrado.DiaLaboral(fechaParticular) == true)
                {
                    if (empleadoEncontrado.EstuvoTrabajandoTalDia(fechaParticular, Asistencias)==null)
                    {
                        contadorInasistencia++;
                    }
                }
                fechaParticular = fechaParticular.AddDays(1);
            }
            return contadorInasistencia;
        }
       


        //TARDANZA Listo--------------------------------------------------
        public int ContadorTardanza(string legajo, DateTime fechaDesde, DateTime FechaHasta)
        {
            Empleado empleadoEncontrado = ObtenerEmpleado(legajo);
            DateTime fechaParticular = fechaDesde;
            int contadorTardanza = 0;
            while (fechaParticular >= fechaDesde & fechaParticular <= FechaHasta)
            {
                if (empleadoEncontrado.DiaLaboral(fechaParticular) == true)
                {
                    Asistencia asistenciaDeTaldiaDeUnEMpleado = empleadoEncontrado.EstuvoTrabajandoTalDia(fechaParticular, Asistencias);
                    if (asistenciaDeTaldiaDeUnEMpleado != null)
                    {
                        if (asistenciaDeTaldiaDeUnEMpleado.Tardanza(empleadoEncontrado.Turno.Entrada, fechaParticular)==true)
                        {
                            contadorTardanza++;
                        }                    
                    }
                }
                fechaParticular = fechaParticular.AddDays(1);
            }
            return contadorTardanza;
        }


        //contador de extras Listo------------------------------------------------------
        public int ContadorMediasHoraExtra(string legajo, DateTime fechaDesde, DateTime FechaHasta)
        {
            Empleado empleadoEncontrado = ObtenerEmpleado(legajo);
            DateTime fechaParticular = fechaDesde;
            int contadorExtras = 0;
            while (fechaParticular >= fechaDesde & fechaParticular <= FechaHasta)
            {
                if (empleadoEncontrado.DiaLaboral(fechaParticular) == true)
                {
                    Asistencia asistenciaDeTaldiaDeUnEMpleado = empleadoEncontrado.EstuvoTrabajandoTalDia(fechaParticular, Asistencias);
                    if (asistenciaDeTaldiaDeUnEMpleado != null)
                    {
                        contadorExtras += asistenciaDeTaldiaDeUnEMpleado.MediasHorasExtras(empleadoEncontrado.Turno.Entrada,empleadoEncontrado.Turno.Salida, fechaParticular);
            
                    }
                }
                fechaParticular = fechaParticular.AddDays(1);
            }
            return contadorExtras;
        }
       







        //////
        ///INFORMES
        ///
        //00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
        public List<InformeIngreso> RegistroDeAsistenciaUnEmpleado(string nombre, DateTime fechaDesde, DateTime fechaHasta)
        {
            Empleado e = ObtenerEmpleadoPorNombre(nombre);
            string legajo = e.Legajo;
            List<InformeIngreso> resultadoInforme = new List<InformeIngreso>();
            foreach (var item in Asistencias)
            {
                if (item.Ingreso.Date>=fechaDesde.Date & item.Salida.Date <= fechaHasta.Date)
                {
                    if (item.Legajo == legajo)
                    {
                        InformeIngreso nuevoDato = new InformeIngreso();

                        nuevoDato.Legajo = item.Legajo;
                        nuevoDato.Turno = e.Turno.NroTurno;
                        nuevoDato.Fecha = item.Ingreso.Date;
                        nuevoDato.Entrada = "" + item.Ingreso.Hour + ": " + item.Ingreso.Minute;
                        nuevoDato.Salida = "" + item.Salida.Hour + ": " + item.Salida.Minute;
                        nuevoDato.Dia = item.Ingreso.DayOfWeek.ToString();

                        resultadoInforme.Add(nuevoDato);
                    }
                }
            }
            return resultadoInforme;
        }

        public List<Informe> InformeCadaEmpleado(DateTime desde, DateTime hasta)
        {
            List<Informe> nuevoInforme = new List<Informe>();
            DateTime fechaParticular = desde;
            foreach (var item in Empleados)
            {
                Informe nuevoDato = new Informe();

                nuevoDato.Legajo = item.Legajo;
                nuevoDato.Nombre = item.NombreApellido;
                nuevoDato.DiasTrabajados = Asistencias.Count(x => x.Legajo == item.Legajo & x.Ingreso.Date >= desde.Date & x.Ingreso.Date <= hasta.Date);
                nuevoDato.Inasistencias = CantidadInasistencia(item.Legajo, desde, hasta);
                nuevoDato.HorasMediasExtras = ContadorMediasHoraExtra(item.Legajo, desde, hasta);
                nuevoDato.Tardanzas = ContadorTardanza(item.Legajo, desde, hasta);

                nuevoInforme.Add(nuevoDato);

            }
            return nuevoInforme;
        }





    





        











        ///////////
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>


        private void CrearArchivos()
        {
            if (!File.Exists(@"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Empleados.txt"))
                File.Create(@"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Empleados.txt").Close();
            if (!File.Exists(@"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Turnos.txt"))
                File.Create(@"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Turnos.txt").Close();
            if (!File.Exists(@"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Asistencias.txt"))
                File.Create(@"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Asistencias.txt").Close();
        }


        public List<Empleado> ArchivoEmpleados()
        {
            string archivoEmpleado = @"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Empleados.txt";
            using (StreamReader Lector = new StreamReader(archivoEmpleado))
            {
                List<Empleado> empleados = new List<Empleado>();
                string contenido = Lector.ReadToEnd();
                if (contenido != "")
                {
                    empleados = JsonConvert.DeserializeObject<List<Empleado>>(contenido);
                }
                return empleados;
            }
        }

        public bool GuardarEmpleados()
        {
            string archivoEmpleados = @"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Empleados.txt";
            using (StreamWriter Escritura = new StreamWriter(archivoEmpleados, false))
            {
                string Contenido = JsonConvert.SerializeObject(Empleados);
                Escritura.Write(Contenido);
                return true;
            }
        }

        public List<Turno> ArchivoTurnos()
        {
            string archivoTurnos = @"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Turnos.txt";
            using (StreamReader Lector = new StreamReader(archivoTurnos))
            {
                List<Turno> turnos = new List<Turno>();
                string contenido = Lector.ReadToEnd();
                if (contenido != "")
                {
                    turnos = JsonConvert.DeserializeObject<List<Turno>>(contenido);
                }
                return turnos;
            }
        }

        public bool GuardarTurnos()
        {
            string archivoTurno = @"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Turnos.txt";
            using (StreamWriter Escritura = new StreamWriter(archivoTurno, false))
            {
                string Contenido = JsonConvert.SerializeObject(Turnos);
                Escritura.Write(Contenido);
                return true;
            }
        }

        public List<Asistencia> ArchivoAsistencias()
        {
            string archivoAsistencia = @"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Asistencias.txt";
            using (StreamReader Lector = new StreamReader(archivoAsistencia))
            {
                List<Asistencia> asistencias = new List<Asistencia>();
                string contenido = Lector.ReadToEnd();
                if (contenido != "")
                {
                    asistencias = JsonConvert.DeserializeObject<List<Asistencia>>(contenido);
                }
                return asistencias;
            }
        }

        public bool GuardarAsistencias()
        {
            string archivoAsistencia = @"C:\Users\NICOLAS DOMINGUEZ\Documents\UNIVERSIDAD\2DO AÑO\PROGRAMACION\TPN2_Delbono_Dominguez\2020-prog1-tp2\Archivos\Asistencias.txt";
            using (StreamWriter Escritura = new StreamWriter(archivoAsistencia, false))
            {
                string Contenido = JsonConvert.SerializeObject(Asistencias);
                Escritura.Write(Contenido);
                return true;
            }
        }


        













    }
}
