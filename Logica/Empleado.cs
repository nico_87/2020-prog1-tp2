﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado
    {
        public string Legajo { get; set; }
        public string NombreApellido { get; set; }
        public string Domicilio { get; set; }
        public string Localidad { get; set; }
        public string Telefono { get; set; }
        public string CorreoElec { get; set; }
        public double SueldoNeto { get; set; }
        public Turno Turno { get; set; }
        public int Estado { get; set; }//0:ACTIVO -- 99:ELIMINADO

        //returna true si esta trabajando
        public bool EstaTrabajando(List<Asistencia> Asistencias,ref bool yaIngreso)
        {
            foreach (var item in Asistencias)
            {
                if (item.Ingreso.Date == DateTime.Today)//nos fijamos los datos de hoy
                {
                    if (item.Legajo == Legajo)//preguntamos si tiene registro hoy
                    {
                        if (item.Salida<DateTime.Today)//si ingreso y no salio
                        {
                            item.Salida = DateTime.Now;
                            return true;//esta trabajando
                        }
                        else
                        {
                            yaIngreso = true;
                            return true;
                        }
                    }                   
                }
            }
            return false;//no hay ningun registro de asistencia
        }

        //si el empleado debe trabajar en la fecha indicada por parametro returna true
        public bool DiaLaboral(DateTime fechaParticular)
        {
            int dia = Convert.ToInt32(fechaParticular.DayOfWeek);
            foreach (var item in Turno.Dias)
            {
                if (item == dia)
                {
                    return true;
                }
            }
            return false;
        }

        internal void ModificarEmpleado(string nombre, string domicilio, string localidad, string telefono, string correo, double sueldo)
        {
            NombreApellido = nombre;
            Domicilio = domicilio;
            Localidad = localidad;
            Telefono = telefono;
            SueldoNeto = sueldo;
            CorreoElec = correo;
        }

        //si el empleado trabajo returna true
        internal Asistencia EstuvoTrabajandoTalDia(DateTime fechaParticular, List<Asistencia> asistencias)
        {
            foreach (var item in asistencias)
            {
                if (item.Ingreso.Date == fechaParticular.Date)//nos fijamos los datos de esa fecha
                {
                    if (item.Legajo == Legajo)//preguntamos si tiene registro hoy
                    {
                        return item;
                    }
                }
            }
            return null;
        }

    }
}
