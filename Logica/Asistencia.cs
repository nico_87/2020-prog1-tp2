﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Asistencia
    {
        public DateTime Ingreso { get; set; }
        public DateTime Salida { get; set; }
        public string Legajo { get; set; }

        //devuelve true si hay tardanza
        internal bool Tardanza(int entrada, DateTime fechaParticular)
        {
            DateTime fechaHoraIngreso = fechaParticular;
            fechaHoraIngreso = fechaHoraIngreso.AddHours(entrada);

            TimeSpan calculoDiferencia = (fechaHoraIngreso - Ingreso);
            double minutosTotales = calculoDiferencia.TotalMinutes; //ver que me tome solo la hora y no las fechas de los turnos
            if (minutosTotales <= -15)
            {
                return true;
            }
            return false;
        }

        //calculas las medias horas extras tanto como retiros tempranos
        public int MediasHorasExtras(int entrada, int salida, DateTime fechaParticular)
        {
            DateTime fechaHoraIngreso = fechaParticular;
            fechaHoraIngreso = fechaHoraIngreso.AddHours(entrada);
            TimeSpan diferenciaEntrada = (fechaHoraIngreso - Ingreso );
            double minutosIngreso = diferenciaEntrada.TotalMinutes;
            int mediaExtraIngreso = 0;
            if (minutosIngreso>0)
            {
                mediaExtraIngreso = Convert.ToInt32(minutosIngreso / 30);
            }

            DateTime fechaHoraSalida = fechaParticular;
            fechaHoraSalida = fechaHoraSalida.AddHours(salida);
            TimeSpan diferenciaSalida = (Salida - fechaHoraSalida );
            double minutosSalida = diferenciaSalida.TotalMinutes;
            int mediaExtraSalida = Convert.ToInt32(minutosSalida / 30);//Aca ya resta las horas si se ha retitado antes

            return mediaExtraIngreso + mediaExtraSalida;

        }
    
    }
}
