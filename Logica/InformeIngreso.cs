﻿using System;

namespace Logica
{
    public class InformeIngreso
    {
        public int Turno { get; set; }
        public string Legajo { get; internal set; }
        public DateTime Fecha { get; set; }
        public string Dia { get; set; }
        public string Entrada { get; internal set; }
        public string Salida { get; internal set; }

    }
}