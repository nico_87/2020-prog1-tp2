﻿using System;
using Logica;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormTP2
{
    public partial class Form1 : Form
    {
        private Principal principal;

        public Form1()
        {
            InitializeComponent();
            principal = new Principal();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = principal.ListadoEmpleadosNombr();
                 
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            principal.AltaEmpleado("nombre", "domicilio", "localidad", "telefono", "correoe", 100, 1, 0);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string dato = textBox1.Text;
            List<string> Res = principal.BusquedaEmpeladoParcialOLegajo(null,dato);
            grillaPrincipal.AutoGenerateColumns = true;
            grillaPrincipal.DataSource = null;
            grillaPrincipal.DataSource = Res;
        }

        private void buttonRegistrarAsistencia_Click(object sender, EventArgs e)
        {
            principal.RegistrarAsistencia(comboBox1.Text,true);
            DateTime desde = new DateTime(2020,10,10);
            List<InformeIngreso> registro = principal.RegistroDeAsistenciaUnEmpleado(comboBox1.Text, desde, DateTime.Now);
            grillaPrincipal.AutoGenerateColumns = true;
            grillaPrincipal.DataSource = null;
            grillaPrincipal.DataSource = registro;
        }

        private void grillaPrincipal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        
    }
}
