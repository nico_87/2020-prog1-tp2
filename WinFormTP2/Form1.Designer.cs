﻿namespace WinFormTP2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.grillaPrincipal = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonRegistrarAsistencia = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grillaPrincipal)).BeginInit();
            this.SuspendLayout();
            // 
            // grillaPrincipal
            // 
            this.grillaPrincipal.AllowUserToAddRows = false;
            this.grillaPrincipal.AllowUserToDeleteRows = false;
            this.grillaPrincipal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaPrincipal.Location = new System.Drawing.Point(30, 95);
            this.grillaPrincipal.Name = "grillaPrincipal";
            this.grillaPrincipal.ReadOnly = true;
            this.grillaPrincipal.Size = new System.Drawing.Size(611, 244);
            this.grillaPrincipal.TabIndex = 0;
            this.grillaPrincipal.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grillaPrincipal_CellContentClick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(925, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Empleados";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Nico",
            "Jil"});
            this.comboBox1.Location = new System.Drawing.Point(30, 51);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // buttonRegistrarAsistencia
            // 
            this.buttonRegistrarAsistencia.Location = new System.Drawing.Point(179, 21);
            this.buttonRegistrarAsistencia.Name = "buttonRegistrarAsistencia";
            this.buttonRegistrarAsistencia.Size = new System.Drawing.Size(190, 53);
            this.buttonRegistrarAsistencia.TabIndex = 6;
            this.buttonRegistrarAsistencia.Text = "Registrar Ingreso / Salida";
            this.buttonRegistrarAsistencia.UseVisualStyleBackColor = true;
            this.buttonRegistrarAsistencia.Click += new System.EventHandler(this.buttonRegistrarAsistencia_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 424);
            this.Controls.Add(this.buttonRegistrarAsistencia);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.grillaPrincipal);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaPrincipal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grillaPrincipal;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonRegistrarAsistencia;
    }
}

