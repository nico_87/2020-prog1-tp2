﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace InformeEmpleado1
{
    public partial class InformeTodosEmpleados : Form
    {
        private Principal principal;

        public InformeTodosEmpleados()
        {
            InitializeComponent();
            principal = new Principal();
        }

        private void Form1_Load(object sender, EventArgs e)
        {



        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Informe> registro = principal.InformeCadaEmpleado(Convert.ToDateTime(dateTimePicker1.Text),Convert.ToDateTime(dateTimePicker2.Text));
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = registro;
        }
    }
}
