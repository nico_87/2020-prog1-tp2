﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace InformeXEmpleado
{
    public partial class RegistroEmpleadoAsis : Form
    {
        private Principal principal;

        public RegistroEmpleadoAsis()
        {
            InitializeComponent();
            principal = new Principal();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBoxEmpleados.DataSource = principal.ListadoEmpleadosNombr();
        }

        private void buttonRegistrar_Click(object sender, EventArgs e)
        {
            bool yaTrabajo = false; bool noDebeTrabajar = false;
            principal.RegistrarAsistencia(comboBoxEmpleados.Text, ref noDebeTrabajar, ref yaTrabajo);
            if (yaTrabajo==true)
            {
                MessageBox.Show("El empleado ya ingreso y salio hoy");
            }
            if (noDebeTrabajar==true)
            {
                MessageBox.Show("El empleado no debe trabajar hoy");
            }
            DateTime desde = new DateTime(2020, 10, 10);
            List<InformeIngreso> registro = principal.RegistroDeAsistenciaUnEmpleado(comboBoxEmpleados.Text, desde, DateTime.Now);
            grillaInformeXEmpleado.AutoGenerateColumns = true;
            grillaInformeXEmpleado.DataSource = null;
            grillaInformeXEmpleado.DataSource = registro;
        }


    }
}
