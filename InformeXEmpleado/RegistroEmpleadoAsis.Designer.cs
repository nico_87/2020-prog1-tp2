﻿namespace InformeXEmpleado
{
    partial class RegistroEmpleadoAsis
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxEmpleados = new System.Windows.Forms.ComboBox();
            this.grillaInformeXEmpleado = new System.Windows.Forms.DataGridView();
            this.buttonRegistrar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grillaInformeXEmpleado)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxEmpleados
            // 
            this.comboBoxEmpleados.FormattingEnabled = true;
            this.comboBoxEmpleados.Location = new System.Drawing.Point(67, 33);
            this.comboBoxEmpleados.Name = "comboBoxEmpleados";
            this.comboBoxEmpleados.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEmpleados.TabIndex = 0;
            // 
            // grillaInformeXEmpleado
            // 
            this.grillaInformeXEmpleado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaInformeXEmpleado.Location = new System.Drawing.Point(67, 78);
            this.grillaInformeXEmpleado.Name = "grillaInformeXEmpleado";
            this.grillaInformeXEmpleado.Size = new System.Drawing.Size(643, 227);
            this.grillaInformeXEmpleado.TabIndex = 1;
            // 
            // buttonRegistrar
            // 
            this.buttonRegistrar.Location = new System.Drawing.Point(246, 12);
            this.buttonRegistrar.Name = "buttonRegistrar";
            this.buttonRegistrar.Size = new System.Drawing.Size(222, 42);
            this.buttonRegistrar.TabIndex = 2;
            this.buttonRegistrar.Text = "Registra Asistencia y Generar Informe";
            this.buttonRegistrar.UseVisualStyleBackColor = true;
            this.buttonRegistrar.Click += new System.EventHandler(this.buttonRegistrar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonRegistrar);
            this.Controls.Add(this.grillaInformeXEmpleado);
            this.Controls.Add(this.comboBoxEmpleados);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaInformeXEmpleado)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxEmpleados;
        private System.Windows.Forms.DataGridView grillaInformeXEmpleado;
        private System.Windows.Forms.Button buttonRegistrar;
    }
}

